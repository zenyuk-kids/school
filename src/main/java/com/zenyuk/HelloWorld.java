package com.zenyuk;

public class HelloWorld {
    public static void main(String[] args) {
        Boy spencer = new Boy();
        spencer.name = "Spencer-Bencer";
        spencer.clothesColor = "chocolate";
        spencer.hairLength = "middle";
        spencer.height = 120;

        System.out.println(spencer.name);
        System.out.println(spencer.height);


        Girl nadia = new Girl();
        nadia.age = 14;
        nadia.eyeColor = "black";
        nadia.name = "Nadia";
        nadia.hairColor = "brown";
        nadia.noseSize = "ExtraHuge";

        System.out.println(nadia.name);
        System.out.println(nadia.eyeColor);
    }
}
